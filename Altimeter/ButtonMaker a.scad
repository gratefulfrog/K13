$fn=100;

/////////// TARGETS /////////////////////

//button(3);   // m3
button(2.5); // m2.5
//translate([0,0,-buttonLowerZ/2])
//insert(2.5,hole = true);
////////////////////////////////////////////////

buttonUpperZ = 10;
buttonUpperD = 20;
//buttonLowerD = 16;
buttonLowerD = 14.3024;
buttonLowerZ = 5;

buttonAxisL       = buttonLowerZ;
buttonAxisD       = 4;
buttonFlatX       = 2+1.6;
buttonFlatEpsilon = 0.1;


// m3 insert m3x4x5
// m3 grubb screw m3x3 m3x4 m3x10
insertLm3        = 4; // true dimension
insertDm3        = 5; // true dimension
insertHoleDm3    = 4.3 +0.2; //mounting hole

// m2.5 insert  m2.5x4x3.5
// m2.5 grubb screw m2.5x4 or m2.5x5
insertLm2_5        = 4; // true dimension
insertDm2_5        = 3.6; // true dimension
insertHoleDm2_5    = 3.15+0.15; // .15 epsilon diam

insertXOffset = 1;

module line(sens=1){
  linear_extrude(10,twist=sens*45,convexity = 10, center=true)
    translate([10,0,0])
      square(1,center=true);
}
module lines (sens=1){
  for (i=[0:15:360])
    rotate([0,0,i])
      line(sens);
}

module buttonUpper(){
  difference(){
    cylinder(h=10,d=20,center=true);
    lines();
    lines(-1);
  }
}
//buttonUpper();

module axis(){
  sqD       = 2*buttonAxisD;
  cubeTrans = [sqD/2 + buttonFlatX - buttonAxisD/2,0,0];
  difference(){
    cylinder(h=buttonAxisL,d=buttonAxisD,center=true);
    translate(cubeTrans)
      linear_extrude(height=buttonAxisL,center=true)
        square(sqD,center=true);
  }
}
//axis();

module axisCutter(){
  sqD       = 2*buttonAxisD;
  cubeTrans = [sqD/2 + buttonFlatEpsilon + buttonFlatX - buttonAxisD/2,0,0];
  difference(){
    cylinder(h=buttonAxisL,d=buttonAxisD+2*buttonFlatEpsilon,center=true);
    translate(cubeTrans)
      linear_extrude(height=buttonAxisL,center=true)
        square(sqD,center=true);
  }
}
//axisCutter();

module insert (m3=true,hole=false){
  insertL     = (m3 ? insertLm3 : insertLm2_5);
  insertD     = (m3 ? insertDm3 : insertDm2_5);
  insertHoleD = (m3 ? insertHoleDm3 : insertHoleDm2_5);
  xTrans = buttonFlatEpsilon + buttonFlatX - buttonAxisD/2 + insertXOffset;
  translate([xTrans,0,0])
    rotate([0,90,0])
      cylinder(h=(hole ? buttonLowerD/2 : insertL),d=(hole ? insertHoleD: insertD),center=false);
}
if ($preview){
  color("green",0.2)
    insert(false);
  color("red",0.2)
    insert(true);
}
//insert(true);

module bolt (mNumber=3){
  dia = mNumber+0.25;
    rotate([0,90,0])
      cylinder(h=buttonLowerD/2,d=dia,center=false);
}
//bolt();

module buttonLower(mNumber=3){
  difference(){
    cylinder(h=buttonAxisL,d=buttonLowerD,center=true);
    axisCutter();
    insert(mNumber==3,hole=true);
    bolt(mNumber);
  }
}
/*
M = 2.5;
if($preview)
  color("blue",0.2)
    buttonLower(M);
*/

module button(m){
  translate([0,0,buttonUpperZ/2])
    buttonUpper();
  translate([0,0,-buttonLowerZ/2])
    buttonLower(m);
}
//button(3);
    