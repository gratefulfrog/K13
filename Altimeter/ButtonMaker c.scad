$fn=100;

/////////// TARGETS /////////////////////

//button(3);   // m3
button(2.5); // m2.5
//translate([0,0,-buttonLowerZ/2])
//insert(2.5,hole = true);
////////////////////////////////////////////////

boltM3TrueD   = 3;
boltM2_5TrueD = 2.5;
boltDEpsilon  = 0.2;

function botlHoleD(trueD) = trueD+boltDEpsilon;

buttonAxisTrueLength = 5;
buttonMinCoverZ      = 1;

buttonUpperZ = 7;// 10;
buttonUpperD = 20;
//buttonLowerD = 16;
buttonLowerD = 14.3024;
buttonLowerZ = buttonAxisTrueLength+buttonMinCoverZ + 1; //5;

buttonAxisL       = buttonLowerZ;
buttonAxisD       = 4.2;
buttonFlatX       = 2+1.6;
buttonFlatEpsilon = 0.1;


// m3 insert m3x4x5
// m3 grubb screw m3x3 m3x4 m3x10
insertLm3        = 4; // true dimension
insertDm3        = 5; // true dimension
insertHoleDm3    = 4.3 +0.2; //mounting hole
//boltM3D          = 3.2;

// m2.5 insert  m2.5x4x3.5
// m2.5 grubb screw m2.5x4 or m2.5x5
insertLm2_5        = 4; // true dimension
insertDm2_5        = 3.6; // true dimension
insertHoleDm2_5    = 3.15+0.15; // .15 epsilon diam
//boltM2_5D          = 2.65;

insertXOffset = 1;

function buttonZAlign(boltTrueD) = buttonAxisTrueLength-buttonMinCoverZ - boltTrueD/2;
echo("Z for m3",buttonZAlign(boltM3TrueD));
echo("Z for m2.5",buttonZAlign(boltM2_5TrueD));

module line(sens=1){
  linear_extrude(buttonUpperZ,twist=sens*45,convexity = 10, center=true)
    translate([buttonUpperD/2,0,0])
      square(1,center=true);
}
module lines (sens=1){
  for (i=[0:15:360])
    rotate([0,0,i])
      line(sens);
}

module buttonUpper(){
  difference(){
    cylinder(h=buttonUpperZ,d=buttonUpperD,center=true);
    if (!$preview){
      lines();
      lines(-1);
    }
  }
}
//buttonUpper();

module axis(){
  sqD       = 2*buttonAxisD;
  cubeTrans = [sqD/2 + buttonFlatX - buttonAxisD/2,0,0];
  difference(){
    cylinder(h=buttonAxisL,d=buttonAxisD,center=true);
    translate(cubeTrans)
      linear_extrude(height=buttonAxisL,center=true)
        square(sqD,center=true);
  }
}
//axis();

module axisCutter(){
  sqD       = 2*buttonAxisD;
  cubeTrans = [sqD/2 + buttonFlatEpsilon + buttonFlatX - buttonAxisD/2,0,0];
  difference(){
    cylinder(h=buttonAxisL,d=buttonAxisD+2*buttonFlatEpsilon,center=false);
    translate(cubeTrans)
      linear_extrude(height=buttonAxisL,center=false)
        square(sqD,center=true);
  }
}
//axisCutter();

module insert (m3=true,hole=false){
  insertL     = (m3 ? insertLm3 : insertLm2_5);
  insertD     = (m3 ? insertDm3 : insertDm2_5);
  insertHoleD = (m3 ? insertHoleDm3 : insertHoleDm2_5);
  xTrans = buttonFlatEpsilon + buttonFlatX - buttonAxisD/2 + insertXOffset;
  translate([xTrans,0,0])
    rotate([0,90,0])
      cylinder(h=(hole ? buttonLowerD/2 : insertL),d=(hole ? insertHoleD: insertD),center=false);
}
/*
if ($preview){
  color("green",0.8)
    insert(false);
  color("red",0.8)
    insert(false,true);
}
//insert(false);
*/

module bolt (mNumber=3){
  dia = botlHoleD(mNumber); //+0.25;
    rotate([0,90,0])
      cylinder(h=buttonLowerD/2,d=dia,center=false);
}
//bolt();

module buttonLower(mNumber=3){
  boltD = (mNumber== 3 ? boltM3TrueD : boltM2_5TrueD);
  translate([0,0,buttonZAlign(boltD)-buttonLowerZ/2])
  difference(){
    translate([0,0,-buttonZAlign(boltD)])
    if ($preview)
      color("red",0.2)
        cylinder(h=buttonLowerZ,d=buttonLowerD,center=false);
    else
      cylinder(h=buttonLowerZ,d=buttonLowerD,center=false);
      translate([0,0,-buttonZAlign(boltD)])
        axisCutter();
      insert(mNumber==3,hole=true);
      bolt(mNumber);
  }
}
/*
M=3;
buttonLower(M);
*/
/*
M = 2.5;
if($preview)
  color("blue",0.2)
    buttonLower(M);
else
  buttonLower(M);
*/
/*
//translate([0,0,(buttonZAlign(2.5))]){
        axisCutter();
        insert(mNumber==3,hole=true);
        bolt(mNumber);
*/
module button(m){
  translate([0,0,buttonUpperZ/2])
    buttonUpper();
  translate([0,0,-buttonLowerZ/2])
    buttonLower(m);
}
/*
mM = 3;
if ($preview){
  color("red",0.2)
    button(mM);
//  translate([0,0,-
  }
else
  button(mM);
*/
    